@extends('layouts.app')

@section('page-level-styles')
    <style>
        .w-40 {
            max-width: 40px;
        }
    </style>
@endsection

@section('main-content')
    <div class="container">
        <div class="row mt-4">
            <div class="col-7">
                <img src="{{ asset('/storage/' . $post->image) }}" alt="Post" class="w-100">
            </div>
            <div class="col-5">
                <div class="d-flex align-items-center">
                    <div class="pr-3">
                        <img src="{{ $user->profile->image == $user->gravatar_image ? $user->gravatar_image : asset('/storage/' . $user->profile->image) }}" alt="profile-image" class="rounded-circle w-100 w-40">
                    </div>
                    <div>
                        <div class="font-weight-bold">
                            <a href="{{ route('profile.show', ['user' => $user]) }}">
                                <span class="text-dark">{{ $user->username }}</span>
                            </a>
                            @if($user->id != auth()->id())
                                <button class="btn btn-sm mr-auto ml-2 {{ $follows ? 'btn-outline-primary' : 'btn-primary' }}" id="followButton" onclick="follow({{ $user->id}})">{{ $follows ? 'Unfollow' : 'Follow' }}</button>
                            @endif
                        </div>
                    </div>
                </div>
                <hr>
                <p>
                    <span class="font-weight-bold">
                        <a href="{{ route('profile.show', ['user' => $user]) }}">
                            <span class="text-dark">{{ $post->user->username }}</span>
                        </a>
                    </span> {{ $post->caption }}
                </p>
                <small>Post Created: {{ $post->created_at->diffForHumans() }}</small>
            </div>
        </div>
    </div>
@endsection
