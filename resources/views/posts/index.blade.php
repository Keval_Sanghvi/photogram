@extends('layouts.app')

@section('page-level-styles')
    <style>
        .w-60 {
            max-width: 60px;
        }
    </style>
@endsection

@section('main-content')
    <div class="container">
        @foreach ($posts as $post)
            <div class="post mt-5 mb-5">
                <div class="row">
                    <div class="col-12 mb-3">
                        <div class="d-flex align-items-center">
                            <div class="pr-3">
                                <img src="{{ $post->user->profile->image == $post->user->gravatar_image ? $post->user->gravatar_image : asset('/storage/' . $post->user->profile->image) }}" alt="profile-image" class="rounded-circle w-100 w-60">
                            </div>
                            <div>
                                <div class="font-weight-bold">
                                    <a href="{{ route('profile.show', ['user' => $post->user]) }}">
                                        <span class="text-dark">{{ $post->user->username }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <a href="{{ route('profile.show', ['user' => $post->user]) }}">
                            <img src="{{ asset('/storage/' . $post->image) }}" alt="Post" class="w-100">
                        </a>
                    </div>
                    <div class="col-12 mt-3">
                        <p>
                            <span class="font-weight-bold">
                                <a href="{{ route('profile.show', ['user' => $post->user]) }}">
                                    <span class="text-dark">{{ $post->user->username }}</span>
                                </a>
                            </span> {{ $post->caption }}
                        </p>
                        <small>Post Created: {{ $post->created_at->diffForHumans() }}</small>
                    </div>
                </div>
            </div>
            <hr>
        @endforeach
        @if($posts->isEmpty())
            <div class="row">
                <div class="col-12 mt-5">
                    <div class="alert text-center alert-primary" role="alert">
                        Follow Users To View Their Posts!
                    </div>
                </div>
            </div>
        @endif
        <div class="mt-5">
            {{ $posts->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>
@endsection
