<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand my-font" href="{{ route('posts.index') }}">Photogram</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    @auth
                        <a class="nav-link dropdown-toggle my-font" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ auth()->user()->username }}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a href="{{ route('profile.show', auth()->user()) }}" class="dropdown-item">View Profile</a>
                            <a class="dropdown-item" href="{{ route('posts.index') }}">Home</a>
                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <input type="submit" class="dropdown-item" value="Logout">
                            </form>
                        </div>
                    @else
                        <a href="{{ route('login') }}" class="btn btn-sm btn-primary mr-3">Log in</a>
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="btn btn-sm btn-primary mr-3">Register</a>
                        @endif
                    @endauth
                </li>
            </ul>
        </div>
    </div>
</nav>
