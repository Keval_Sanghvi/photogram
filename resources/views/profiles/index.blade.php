@extends('layouts.app')

@section('page-level-styles')
    <style>
        .w-10 {
            width: 10%;
        }

        .image {
            display: block;
            margin: 0 auto;
        }

        a.deco-none {
            color: inherit;
            text-decoration:none;
        }
    </style>
@endsection

@section('main-content')
    <div class="container">
        @if($feature == "followers")
            <div class="row mt-4 mb-5">
                <div class="col-12 text-center">
                    <h1>{{ $user->username }}'s Followers</h1>
                </div>
            </div>
            @foreach($followers as $follower)
                <div class="row mt-4">
                    <div class="col-6">
                        <img src="{{ $follower->profile->image == $follower->gravatar_image ? $follower->gravatar_image : asset('/storage/' . $follower->profile->image) }}" alt="profile-image" class="rounded-circle img-fluid w-10 image">
                    </div>
                    <div class="col-6">
                        <a href="{{ route('profile.show', $follower) }}" class="deco-none h2">{{ $follower->username }}</a>
                    </div>
                </div>
                <hr>
            @endforeach
            @if($followers->isEmpty())
                <div class="row">
                    <div class="col-12 mt-5">
                        <div class="alert text-center alert-primary" role="alert">
                            This User Has No Followers!
                        </div>
                    </div>
                </div>
            @endif
        @elseif($feature == "following")
            <div class="row mt-4 mb-5">
                <div class="col-12 text-center">
                    <h1>{{ $user->username }}'s Following</h1>
                </div>
            </div>
            @foreach($followings as $following)
                <div class="row mt-4">
                    <div class="col-6">
                        <img src="{{ $following->image == $following->user->gravatar_image ? $following->user->gravatar_image : asset('/storage/' . $following->image) }}" alt="profile-image" class="rounded-circle img-fluid w-10 image">
                    </div>
                    <div class="col-6">
                        <a href="{{ route('profile.show', $following) }}" class="deco-none h2">{{ $following->user->username }}</a>
                    </div>
                </div>
                <hr>
            @endforeach
            @if($followings->isEmpty())
                <div class="row">
                    <div class="col-12 mt-5">
                        <div class="alert text-center alert-primary" role="alert">
                            This User Does Not Follow Anyone!
                        </div>
                    </div>
                </div>
            @endif
        @endif
    </div>
@endsection
