@extends('layouts.app')

@section('page-level-styles')
    <style>
        .my-font {
            font-family: 'Noto Sans SC', sans-serif;
        }

        .image {
            display: block;
            max-width: 60%;
            margin: 0 auto;
        }

        .w-70 {
            width: 70%;
        }

        hr {
            background-color: #dddddd;
        }

        a.deco-none {
            color: inherit;
            text-decoration:none;
        }
    </style>
@endsection

@section('main-content')
    <div class="container">
        <div class="row mt-2">
            <div class="col-12">
                @include('layouts.partials._message')
            </div>
        </div>
        <div class="row mt-4 mb-4">
            <div class="col-3">
                <img src="{{ $user->profile->image == $user->gravatar_image ? $user->gravatar_image : asset('/storage/' . $user->profile->image) }}" alt="profile-image" class="rounded-circle img-fluid image">
            </div>
            <div class="col-9">
                <div class="ml-5">
                    <div class="d-flex align-items-center">
                        <h3 class="font-weight-light">{{ $user->username }}</h3>
                        @if($user->id == auth()->id())
                            <button class="btn btn-sm mr-auto ml-2">You</button>
                        @else
                            <button class="btn btn-sm mr-auto ml-2 {{ $follows ? 'btn-outline-primary' : 'btn-primary' }}" id="followButton" onclick="follow({{ $user->id}})">{{ $follows ? 'Unfollow' : 'Follow' }}</button>
                        @endif
                        @can('update', $user->profile)
                            <a href="{{ route('posts.create') }}" class="btn btn-primary btn-sm mr-2">New Post</a>
                        @endcan
                        @can('update', $user->profile)
                            <a href="{{ route('profile.edit', ['user' => $user]) }}" class="btn btn-secondary btn-sm">Edit Profile</a>
                        @endcan
                    </div>
                    <div class="d-flex">
                        <div class="pr-5">
                            <strong>{{ $user->getPostsCount() }}</strong> posts
                        </div>
                        <div class="pr-5">
                            <a href="{{ route('profile.followers', ['user' => $user]) }}" class="deco-none"><strong>{{ $followersCount }}</strong> followers</a>
                        </div>
                        <div class="pr-5">
                            <a href="{{ route('profile.following', ['user' => $user]) }}" class="deco-none"><strong>{{ $followingCount }}</strong> following</a>
                        </div>
                    </div>
                    <div class="mt-3 mb-1">
                        {!! $user->profile->title !!}
                    </div>
                    <div class="mt-1">
                        {!! $user->profile->description !!}
                    </div>
                    <p class="mb-2 mt-2"><a href="#">{{ $user->profile->url }}</a></p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            @foreach ($user->posts as $post)
                <div class="col-4 mt-3">
                    <a href="{{ route('posts.show', ['user' => $user, 'post' => $post]) }}">
                        <img class="w-100" src="{{ asset('/storage/' . $post->image) }}" alt="Post">
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('page-level-scripts')
    <script>
        function follow(userId) {
            $.ajax({
                type: 'POST',
                url: `/follow/${userId}`,
                data: {_token: '{{ csrf_token() }}' },
                success: function(data) {
                    if(data.follow) {
                        $("#followButton").text("Unfollow");
                        $("#followButton").addClass('btn-outline-primary');
                        $("#followButton").removeClass('btn-primary');
                        $("#followButton").blur();
                    } else {
                        $("#followButton").text("Follow");
                        $("#followButton").addClass('btn-primary');
                        $("#followButton").removeClass('btn-outline-primary');
                        $("#followButton").blur();
                    }
                },
                error: function(data) {
                    if(data.status == 401) {
                        let url = "{{ route('login') }}";
                        document.location.href = url;
                    }
                }
            });
        }
    </script>
@endsection
