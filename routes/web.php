<?php

use App\Http\Controllers\FollowsController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\ProfilesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::resource('posts', PostsController::class)->except('show', 'index');
Route::get('/', [PostsController::class, 'index'])->name('posts.index');
Route::get('/profile/{user}/posts/{post}', [PostsController::class, 'show'])->name('posts.show');

Route::get('/profile/{user}', [ProfilesController::class, 'show'])->name('profile.show');
Route::get('/profile/{user}/edit', [ProfilesController::class, 'edit'])->name('profile.edit');
Route::put('/profile/{user}', [ProfilesController::class, 'update'])->name('profile.update');
Route::get('/profile/{user}/followers', [FollowsController::class, 'followers'])->name('profile.followers');
Route::get('/profile/{user}/following', [FollowsController::class, 'following'])->name('profile.following');

Route::post('follow/{user}', [FollowsController::class, 'store'])->name('profile.follow');

require __DIR__.'/auth.php';
