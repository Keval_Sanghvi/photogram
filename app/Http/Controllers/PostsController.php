<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(): View
    {
        $users = auth()->user()->following()->pluck('profiles.user_id');
        $posts = Post::whereIn('user_id', $users)->with('user')->latest()->paginate(5);
        return view('posts.index', compact(['posts']));
    }

    public function create(): View
    {
        return view('posts.create');
    }

    public function store(CreatePostRequest $request): RedirectResponse
    {
        $imagePath = $request->file('image')->store('images/posts');
        $image = Image::make(public_path("storage/{$imagePath}"))->fit(1200, 1200);
        $image->save();
        $post = Post::create([
            'caption' => $request->caption,
            'image' => $imagePath,
            'user_id' => auth()->id(),
        ]);
        session()->flash('success', 'Post Created Successfully!');
        return redirect(route('profile.show', ['user' => auth()->user()]));
    }

    public function show(User $user, Post $post): View
    {
        $follows = (auth()->user()) ? auth()->user()->following->contains($user->id) : false;
        return view('posts.show', compact(['user', 'post', 'follows']));
    }
}
