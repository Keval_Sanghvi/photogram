<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\View\View;

class FollowsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(User $user)
    {
        auth()->user()->following()->toggle($user->profile);
        $follows = (auth()->user()) ? auth()->user()->following->contains($user->id) : false;
        if($follows) {
            return response()->json(['follow' => true], 200);
        }
        return response()->json(['follow' => false], 200);
    }

    public function followers(User $user): View
    {
        $followers = $user->profile->followers;
        $feature = "followers";
        return view('profiles.index', compact('user', 'followers', 'feature'));
    }

    public function following(User $user): View
    {
        $followings = $user->following;
        $feature = "following";
        return view('profiles.index', compact('user', 'followings', 'feature'));
    }
}
