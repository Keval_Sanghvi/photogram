<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfileRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;

class ProfilesController extends Controller
{
    public function show(User $user): View
    {
        $follows = (auth()->user()) ? auth()->user()->following->contains($user->id) : false;
        $followersCount = Cache::remember(
            'count.followers.' . $user->id,
            now()->addSeconds(10),
            function() use ($user) {
                return $user->profile->followers->count();
            }
        );
        $followingCount = Cache::remember(
            'count.following.' . $user->id,
            now()->addSeconds(10),
            function () use ($user) {
                return $user->following->count();
            }
        );
        return view('profiles.show', compact(['user', 'follows', 'followersCount', 'followingCount']));
    }

    public function edit(User $user): View
    {
        $this->authorize('update', $user->profile);
        return view('profiles.edit', compact(['user']));
    }

    public function update(UpdateProfileRequest $request, User $user): RedirectResponse
    {
        $this->authorize('update', $user->profile);
        $data = $request->only(['title', 'description', 'url']);
        if($request->hasFile('image')) {
            $imagePath = $request->file('image')->store('images/profiles');
            $image = Image::make(public_path("storage/{$imagePath}"))->fit(1200, 1200);
            $image->save();
            $data['image'] = $imagePath;
        }
        $user->profile->update($data);
        session()->flash('success', 'Profile Updated Successfully!');
        return redirect(route('profile.show', ['user' => auth()->user()]));
    }
}
